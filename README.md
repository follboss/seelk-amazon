Seelk Data Hackathon


L'objectif est de mettre en place un modèle qui prédit lequel des revendeurs de Amazon gagnera le buy_box_owner et d'argumenter le choix des parametres.

Sur le site Amazon, plusieurs marchands vendent souvent le même article. Ces vendeurs sont connus comme revendeurs.
Lorsque plusieurs revendeurs vendent en même temps, il y a soudain une concurrence pour laquelle le vendeur remportera la Buy Box.

le vendeur gagne le Buy Box si seller_name est dans le buy_box_owner field

Après la phase de prétraitement du dataset, je crée un dataframe contenant les parametres utiles à mon modèle :
      [offer]
      [scraping_date]
      [buy_box_owner]
      [seller_name]
      [price_currency]

A la suite de cela je fais une jointure entre le seller_name et le buy_box_owner.
Si le vendeur est dans le buy_box alors dans la nouvelle colonne qui sera seller_name_in_buy_box_owner il recoit '1', sinon il se verra attribué '0'.

Pour mon modèle je recrée un data frame data_preteted: 
      [offer] //le numero d'offre est essentiel pour retrouver le produit et les vendeurs associés 
      [scraping_date] la date est important puisque on peut avoir deux offres avec les memes numeros donc la date est essentiel
      [price] le prix est toujours important si deux vendeurs vendent un meme produits et que l'un le vend moins cher que le second alors le moin cher sera le gagnant
      [seller_name_in_buy_box_owner] et celui ci nous permet de recuperer ceux qui gagne, 1 si gagnant et 0 si perdant.
      

Je choisis le classifieur SGDClassifier qui implémente des modèles linéaires régularisés avec la descente de gradient stochastique.
La descente de gradient stochastique ne prend en compte qu'un point au hasard tout en changeant de poids, contrairement à la descente de gradient qui prend en compte l'ensemble des données d'entraînement.
En tant que tel, la descente de gradient stochastique est beaucoup plus rapide que la descente de gradient lorsqu'il s'agit de grands ensembles de données ce qui est le cas de notre dataset.


Pour notre modèle: 
X_train , y_train , X_val , y_val : données d'apprentissage et de test
Ajout d'hyperparamètre pour l'obtention de meilleur perfomance
ROC : metric pour évaluer mon modèle.

     
 
 
 
      